using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;


public class SaveInput : MonoBehaviour
{
    [SerializeField] TMP_Text nameuser;
    // Start is called before the first frame update
    void Start()
    {
       string texto = PlayerPrefs.GetString("nick");
       nameuser.text = $"Nick: {texto}";
    }
}
