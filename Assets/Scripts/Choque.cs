using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Choque : MonoBehaviour
{
    int numero = 0;
    void OnCollisionEnter(Collision collision)
    {
        
        GameObject objeto1 = GameObject.Find("Player");
        if (collision.gameObject.tag == "Enemigo")
        {
            numero++;
            CuentaChoque(numero); 
        }
    }

    public void CuentaChoque(int numero)
    {
        PlayerPrefs.SetInt("choque", numero);
        Debug.Log(numero);
    }
}
