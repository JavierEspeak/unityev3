﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovEnemigo : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform player;
    private NavMeshAgent Enemy;
    void Start()
    {
        Enemy = GetComponent<NavMeshAgent>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Enemy.destination = player.position;
    }
}
