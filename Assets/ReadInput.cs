using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

public class ReadInput : MonoBehaviour
    { 
    public InputField nombreUsuario; 

    public void ReadStringInput()
    {
        string nombre = "";
        nombre = nombreUsuario.text;
        PlayerPrefs.SetString("nick", nombre);
        PlayerPrefs.SetInt("choque", 0); 
    }

}