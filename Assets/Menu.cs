using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void EscenaName()
    { 
        SceneManager.LoadScene("Name");
    }

    public void EscenaOptions()
    {
        SceneManager.LoadScene("Options");
    }

    public void Salir()
    {
        Application.Quit();
    }

    public void EscenaIniciar()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
