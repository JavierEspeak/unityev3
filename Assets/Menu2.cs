using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu2 : MonoBehaviour
{
    public void EscenaVolver()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Escena1()
    {
        SceneManager.LoadScene("Nivel1");
    }

    public void Escena2()
    {
        SceneManager.LoadScene("Nivel2");
    }

    public void Escena3()
    {
        SceneManager.LoadScene("Nivel3");
    }
}
