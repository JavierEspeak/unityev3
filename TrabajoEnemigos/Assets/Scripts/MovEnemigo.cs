﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovEnemigo : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform player;
    private NavMeshAgent enemigo;
    public bool contagio = false; 
    void Start()
    {
        enemigo = GetComponent<NavMeshAgent>();
        StartCoroutine(persigue()); 
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    IEnumerator persigue()
    {
        while (contagio == false)
        {
            enemigo.destination = player.position;
            yield return new WaitForSeconds(1); 
        }
    }
}
