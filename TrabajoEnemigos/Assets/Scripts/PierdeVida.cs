﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PierdeVida : MonoBehaviour
{
    private int VidaPersonaje = 100;
    private string tipo; 
    // Start is called before the first frame update
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {

        if (hit.collider.tag == "Covid")
        {
            Debug.Log("Coviiid");
            tipo = "covid";
            time(tipo);  
        }

        if (hit.collider.tag == "Pared")
        {
            Debug.Log("Chocaste");
            tipo = "pared";
            time(tipo);
        }
    }

    public IEnumerator time(string tipo)
    {
        if(tipo == "pared")
        {
            VidaPersonaje = VidaPersonaje - 25; 
        }

        if(tipo == "covid")
        {
            VidaPersonaje = VidaPersonaje - 50; 
        }

        Debug.Log("te quedan" + VidaPersonaje); 

        if (VidaPersonaje <= 0)
        {
            Debug.Log("Te Moriste");
        }
        yield return new WaitForSeconds(1); 
    }
}
