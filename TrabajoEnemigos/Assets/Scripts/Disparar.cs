﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{

    void OnCollisionEnter(Collision collision)
    {
        GameObject morira; 

        if (collision.gameObject.tag == "Covid")
        {
            string nombre = collision.transform.gameObject.name;
            morira = GameObject.Find(nombre);
            Destroy(morira); 
        }
    }
}
